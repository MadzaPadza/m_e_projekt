#include "funkcj.h"


float mean(int ilosc_elem, float tablica[])
{
    int i = 0;
    float suma = 0.0;

    for(i=0; i<ilosc_elem; i++)
    {
        suma += tablica[i];
    }

    suma /= ilosc_elem;

    return suma;
}