#include <stdlib.h>
#include <stdio.h>
#include "funkcj.h"






int main()
{
    int n=50;
    
    FILE *plik;// deklaracja wskaźnika plik typu FILE
    //fopen("P0001_attr.rec.txt", "r");//  otwarcie pliku do odczytu

    //Utworzone 3 tablice dynamiczne//
    /*  typ ∗wsk ;
     wsk=(typ ∗) malloc ( ile elemenow ∗ sizeof(typ)); */

    float *array_X;
    array_X = (float *)malloc(n * sizeof(float));
    float *array_Y;
    array_Y = (float *)malloc(n * sizeof(float));
    float *array_RHO;
    array_RHO = (float *)malloc(n * sizeof(float));
    
    //sprawdzenie poprawnego otwarcia pliku:
    if((plik=fopen("P0001_attr.rec.txt", "r"))==NULL)
    
    {
            printf("Nie mozna otworzyc pliku. ");
    }
    else 
    {
        //Pomijam pierwszą linię ze znakami typu char w pliku//
        //CZyli jeśli program napotka coś innego niż /n to kończy skanowanie, a nastepnie dzieki drugiemu /n przechodzi do nowego wiersza w którym są już wartości.
        fscanf(plik, "%*[^\n]\n");
        int i=1;
        //Przechodzę linia po lini i dodaje do odpowiedniej tablicy odpowiadające liczby typu float
        while (fscanf(plik,"%*f\t%f\t%f\t%f\n", &array_X[i], &array_Y[i], &array_RHO[i])==3)
        {
            //Można wyświetlić sobie utworzone tablice//
           /* printf("%d\t", i);
            printf("%f\t", array_X[i]);
            printf("%f\t", array_Y[i]);0
            printf("%f\t", array_RHO[i]);
            printf("\n"); */
            i++;
        }
        
    }
    fclose(plik); 
// obliczenie srednich poszczególnych tablic:
    float sredniaX=mean(n, array_X);
    float sredniaY=mean(n, array_Y);
    float sredniaRHO=mean(n, array_RHO);

    printf("\nWartosc sredniej X: %f", sredniaX);
    printf("\nWartosc sredniej Y: %f", sredniaY);
    printf("\nWartosc sredniej RHO: %f", sredniaRHO);

//obliczenie median poszczególnych tablic:
    float medianaX =  median(n, array_X);
    float medianaY = median(n, array_Y);
    float medianaRHO = median(n, array_RHO);

    printf("\nMediana X wynosi: %f", medianaX);
    printf("\nMediana Y wynosi: %f", medianaY);
    printf("\nMediana RHO wynosi: %f", medianaRHO);

//obliczenie odchyleń standardowych poszczególnych tablic:
    float odchylenieX=standard_deviation(n, array_X, sredniaX);
    float odchylenieY=standard_deviation(n, array_Y, sredniaY);
    float odchylenieRHO=standard_deviation(n, array_RHO, sredniaRHO);

    printf("\nWartosc odchhylenia standardowego wynosi: %f", odchylenieX);
    printf("\nWartosc odchhylenia standardowego wynosi: %f", odchylenieY);
    printf("\nWartosc odchhylenia standardowego wynosi: %f", odchylenieRHO);


//otwieramy ponownie plik aby zapisać w nim wartości sredniej, mediany i odchylenia dla poszczególnych kolumn
    if((plik=fopen("P0001_attr.rec.txt", "a"))==NULL)
    
    {
            printf("Nie mozna otworzyc pliku. ");
    }
    else 
    {
        
        fprintf(plik, "\n\n");
        fprintf(plik,"WARTOSC SREDNIEJ DLA: \nX wynosi: %f \nY wynosi: %f \nRHO wynosi: %f\n",sredniaX,sredniaY,sredniaRHO);
        fprintf(plik,"WARTOSC MEDIANY DLA: \nX wynosi: %f \nY wynosi: %f \nRHO wynosi: %f\n",medianaX,medianaY,medianaRHO);
        fprintf(plik,"WARTOSC ODCHYLENIA STANDARDOWEGO DLA: \nX wynosi: %f \nY wynosi: %f \nRHO wynosi: %f\n",odchylenieX,odchylenieY,odchylenieRHO);
        
    }

    // zamykamy plik
    fclose(plik);

    //zwolnienie 3 tablic dynamicznych
    free(array_X);
    free(array_Y);
    free(array_RHO);


    return 0;

}