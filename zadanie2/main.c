#include <stdlib.h>
#include <stdio.h>
#include "funkcj.h"
#include <string.h>
#define n 50

struct dane
{
    float X[n];
    float Y[n];
    float RHO[n];
    
};

struct stat
{
    float Mean[2];
    float Median[2];
    float Standard_deviation[2];
};

int main()
{
    
    struct dane dane_1;
    struct stat stat_1;
    
    FILE *plik;// deklaracja wskaźnika plik typu FILE
    //fopen("P0001_attr.rec.txt", "r");//  otwarcie pliku do odczytu

    //sprawdzenie poprawnego otwarcia pliku:
    if((plik=fopen("P0001_attr.rec.txt", "r"))==NULL) 
    
    {
            printf("Nie mozna otworzyc pliku. ");
    }
    else 
    {
        //Pomijam pierwszą linię ze znakami typu char w pliku
        //CZyli jeśli program napotka coś innego niż /n to kończy skanowanie, a nastepnie dzieki drugiemu /n przechodzi do nowego wiersza w którym są już wartości.
        fscanf(plik, "%*[^\n]\n");
        int i =0;
        //Przechodzę linia po lini i dodaje do struktury odpowiadające liczby typu float
        while (fscanf(plik,"%*f\t%f\t%f\t%f\n", &dane_1.X[i], &dane_1.Y[i], &dane_1.RHO[i])==3)
        {
            printf("Tablica X: %f\n ",dane_1.X[i]);
            printf("Tablica Y: %f\n ",dane_1.Y[i]);
            printf("Tablica RHO: %f\n ",dane_1.RHO[i]);
            
            i++;
        }


    }
    fclose(plik);

// obliczenie srednich poszczególnych tablic w strukturze:
    float sredniaX=mean(n, dane_1.X);
    float sredniaY=mean(n, dane_1.Y);
    float sredniaRHO=mean(n, dane_1.RHO);

// obliczenie median poszczególnych tablic w strukturze:
    float medianaX =  median(n, dane_1.X);
    float medianaY = median(n, dane_1.Y);
    float medianaRHO = median(n, dane_1.RHO);

//obliczenie odchyleń standardowych poszczególnych tablic w strukturze:
    float odchylenieX=standard_deviation(n, dane_1.X, sredniaX);
    float odchylenieY=standard_deviation(n, dane_1.Y, sredniaY);
    float odchylenieRHO=standard_deviation(n, dane_1.RHO, sredniaRHO);

//przypisanie do odpowiednich zmiennych funkcji zawartych w strukturze stat_1
    stat_1.Mean[0]=sredniaX;
    stat_1.Median[0]=medianaX;
    stat_1.Standard_deviation[0]=odchylenieX;
    stat_1.Mean[1]=sredniaY;
    stat_1.Median[1]=medianaY;
    stat_1.Standard_deviation[1]=odchylenieY;
    stat_1.Mean[2]=sredniaRHO;
    stat_1.Median[2]=medianaRHO;
    stat_1.Standard_deviation[2]=odchylenieRHO;

//tworzymy tablice statystyk
 for(int i = 0; i<3; i++)
 {
     printf("Tablica  %d to: %f\t%f\t%f\n", i ,stat_1.Mean[i], stat_1.Median[i], stat_1.Standard_deviation[i]);
 }

if ((plik = fopen("P0001_attr.rec.txt", "a+"))==NULL)
    printf("Blad otwarcia pliku");
else
{
    char *p;
    char *c = "~~~~";
    rewind(plik); //ustawiamy kursor na początku pliku
    int finded = 0;
    //wczytywanie linia po linii
    char linia[10000] = "";
    while(fgets(linia, 10000, plik))
    {
        p = strstr(linia, c);  //funkcja szuka podciągu szukany_ciąg i ustawia wskaźnik na jego pierwszą literę.
        if(p!=NULL)
        {
           finded = 1;
           break;
        }
    }
    if (finded == 0)//otwieramy ponownie plik aby zapisać w nim wartości sredniej, mediany i odchylenia dla poszczególnych kolumn
    {
        fprintf(plik,"~~~~\n ");
        fprintf(plik,"WARTOSC SREDNIEJ WYNOSI :\n dla X wynosi: %f \n dla Y wynosi:%f \ndla RHO wynosi:%f\n ",sredniaX,sredniaY,sredniaRHO);
        fprintf(plik,"WARTOSC MEDIANY WYNOSI :\n dla X wynosi: %f \n dla Y wynosi:%f \ndla RHO wynosi:%f", medianaX,medianaY,medianaRHO);
        fprintf(plik, "WARTOSC ODCHYLENIA STANDARDOWEGO WYNOSI :\n dla X wynosi: %f \n dla Y wynosi:%f \ndla RHO wynosi:%f\n", odchylenieX,odchylenieY,odchylenieRHO);
        fprintf(plik,"~~~~\n ");
    }
}
fclose(plik);
return 0;

}