#include "funkcj.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

float standard_deviation(int ilosc_elem, float tablica[], float srednia)
{
    int i = 0;
    float odch = 0.0;

    for(i=0; i<ilosc_elem; i++)
    {
        odch += pow((tablica[i] - srednia), 2);         
    }

    odch = sqrt(odch / ilosc_elem);

    return odch;
}