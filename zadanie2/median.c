#include "funkcj.h"
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

float median(int ilosc_elem, float tablica[])
{

    float x=0.0;

    if(ilosc_elem%2==0)
    {

        x=(tablica[(ilosc_elem-1)/2]+tablica[ilosc_elem/2])/2;
        return x;
}
    else
    {
        x=(tablica[(ilosc_elem-1)/2]+tablica[(ilosc_elem+1)]/2)/2;
        return x;
    }
}